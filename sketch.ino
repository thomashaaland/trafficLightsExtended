#include <LiquidCrystal_I2C.h>

// Define some pins
#define LIGHTSENSOR A0
#define LDR_PIN 2

#define NRED 12
#define NYELLOW 11
#define NGREEN 10

#define WRED 7
#define WYELLOW 6
#define WGREEN 5

#define WPEDRED 9
#define WPEDGREEN 8
#define WPEDBTN A2

#define NPEDRED 4
#define NPEDGREEN 3
#define NPEDBTN A1

// LDR Characteristics
const float GAMMA = 0.7;
const float RL10 = 50;

LiquidCrystal_I2C lcd(0x27, 20, 4);

class TrafficLight {
public:
  TrafficLight(int red, int yellow, int green) : red{red}, yellow{yellow}, green{green} 
  {
    pinMode(red, OUTPUT);
    pinMode(yellow, OUTPUT);
    pinMode(green, OUTPUT);
  }

  void setGreen() {
    digitalWrite(red, LOW);
    digitalWrite(yellow, LOW);
    digitalWrite(green, HIGH);
  }

  void setRed2Green() {
    digitalWrite(red, HIGH);
    digitalWrite(yellow, HIGH);
    digitalWrite(green, LOW);
  }

  void setPause() {
    digitalWrite(red, LOW);
    digitalWrite(yellow, LOW);
    digitalWrite(green, LOW);
  }

  void setGreen2Red() {
    digitalWrite(red, LOW);
    digitalWrite(yellow, HIGH);
    digitalWrite(green, LOW);
  }

  void setRed() {
    digitalWrite(red, HIGH);
    digitalWrite(yellow, LOW);
    digitalWrite(green, LOW);
  }
private:
  int red, yellow, green;
} nLight(NRED, NYELLOW, NGREEN), wLight(WRED, WYELLOW, WGREEN);

class PedLight {
public:
  PedLight(int red, int green) : red{red}, green{green} 
  {
    pinMode(red, OUTPUT);
    pinMode(green, OUTPUT);
  }
  void setRed() {
    digitalWrite(red, HIGH);
    digitalWrite(green, LOW);
  }

  void setPause() {
    digitalWrite(red, LOW);
    digitalWrite(green, LOW);
  }

  void setGreen() {
    digitalWrite(red, LOW);
    digitalWrite(green, HIGH);
  }
private:
  int red, green;
} nPed(NPEDRED, NPEDGREEN), wPed(WPEDRED, WPEDGREEN);

class Button {
public:
  Button(int inPin) : btnPin{inPin} 
  {
    pinMode(inPin, INPUT_PULLUP);
  }

  bool pressed() {
    if (digitalRead(btnPin) == LOW) {
      return true;
    } else {
      return false;
    }
  }
private:
  int btnPin;
} nPedBtn(NPEDBTN), wPedBtn(WPEDBTN);

class TrafficIntersection {
public:
  TrafficIntersection(TrafficLight l1, TrafficLight l2, PedLight p1, PedLight p2) 
  : nLight{l1}, wLight{l2}, nPed{p1}, wPed{p2} 
  {
    openTime = 24000;
    openTimeBtnPushed = 3000;
    delayTime = 500;
    transitionLoops = 10;

    nLight.setRed();
    wLight.setGreen();
    nPed.setRed();
    wPed.setGreen();
  }

  void openNorthLane() {
    pedBtnPushed = false;
    for (int i = 0; i < transitionLoops; i++) {
      init_time = millis();
      wPed.setGreen();
      while (millis() < (init_time + delayTime)) {;}
      init_time = millis();
      wPed.setPause();
      while (millis() < (init_time + delayTime)) {;}
    }

    wPed.setRed();
    delay(delayTime);

    nLight.setRed2Green();
    wLight.setGreen2Red();


    init_time = millis();
    while (millis() < (init_time + delayTime)) {
      if (nPedBtn.pressed()) {
        pedBtnPushed = true;
      }
    }
  }

  void holdNorthLane() {
    nLight.setGreen();
    wLight.setRed();

    delay(delayTime);
    nPed.setGreen();

    init_time = millis();
    while (millis() < (init_time + (pedBtnPushed ? openTimeBtnPushed : openTime))) 
    {
      if (nPedBtn.pressed()) {
        pedBtnPushed = true;
      }
    }
  }

  void openWestLane() {
    pedBtnPushed = false;
    for (int i = 0; i < transitionLoops; i++) {
      init_time = millis();
      nPed.setGreen();
      while (millis() < (init_time + delayTime)) {;}
      init_time = millis();
      nPed.setPause();
      while (millis() < (init_time + delayTime)) {;}
    }

    nPed.setRed();
    delay(delayTime);

    nLight.setGreen2Red();
    wLight.setRed2Green();


    init_time = millis();
    while (millis() < (init_time + delayTime)) {
      if (wPedBtn.pressed()) {
        pedBtnPushed = true;
      }
    }
  }

  void holdWestLane() {
    nLight.setRed();
    wLight.setGreen();

    delay(delayTime);
    wPed.setGreen();

    init_time = millis();
    while (millis() < (init_time + (pedBtnPushed ? openTimeBtnPushed : openTime))) 
    {
      if (wPedBtn.pressed()) {
        pedBtnPushed = true;
      }
    }

  }

  void setIdle() {
    nPed.setPause();
    wPed.setPause();
    nLight.setPause();
    wLight.setPause();
    delay(delayTime);
    nLight.setGreen2Red();
    wLight.setGreen2Red();
    delay(delayTime);
  }

private:
  TrafficLight nLight, wLight;
  PedLight nPed, wPed;
  int transitionLoops, openTime, delayTime, openTimeBtnPushed;
  bool pedBtnPushed;
  long int init_time;
} intersection(nLight, wLight, nPed, wPed);

void setup() {
  // put your setup code here, to run once:
  pinMode(LDR_PIN, OUTPUT);

  lcd.init();
  lcd.backlight();

  pinMode(LIGHTSENSOR, INPUT);
  Serial.begin(115200);
}

void loop() {
  // Green for north, Red for west:
  long int time = millis();
  lcd.setCursor(2, 0);
  lcd.print("Room: ");

  int analogValue = analogRead(LIGHTSENSOR);
  float voltage = analogValue / 1024. * 5;
  float resistance = 2000 * voltage / (1 - voltage / 5);
  float lux = pow(RL10 * 1e3 * pow(10, GAMMA) / resistance, (1 / GAMMA));

  lcd.setCursor(0, 0);
  lcd.print("Phase: ");
  if (lux > 3.4) {
    lcd.print("Day");
  } else {
    lcd.print("Night");
  }
  lcd.print("       ");

  if (lux > 3.4) {
    lcd.setCursor(0, 1);
    lcd.print("Transition:  ");
    lcd.print("N/S");
    lcd.print("             ");
    intersection.openNorthLane();
    lcd.setCursor(0, 1);
    lcd.print("Go:          ");
    lcd.print("N/S");
    lcd.print("             ");
    intersection.holdNorthLane();
    lcd.setCursor(0, 1);
    lcd.print("Transition:  ");
    lcd.print("W/E");
    lcd.print("             ");
    intersection.openWestLane();
    lcd.setCursor(0, 1);
    lcd.print("Go:          ");
    lcd.print("W/E");
    lcd.print("             ");
    intersection.holdWestLane();
  } else {
    lcd.setCursor(0, 0);
    lcd.print("Phase: ");
    if (lux > 3.4) {
      lcd.print("Day");
    } else {
      lcd.print("Night");
    }
    lcd.setCursor(0, 1);
    lcd.print("Status:     ");
    lcd.print("Idle");

    intersection.setIdle();
  }
}
