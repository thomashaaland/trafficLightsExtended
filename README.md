# Traffic Lights

This project uses [wokwi](https://wokwi.com) to model a arduino-nano board simulating traffic lights. Pushlights and display are included. To run this project you need to copy paste the code in sketch.ino and sketch.json into wokwi.

![example](trafficLightExtendedDiagram.PNG)

Author: Thomas Haaland
